const userFunctions = require("../user")
const messageFunctions = require("../messages")
const io = require('socket.io')(8080);

const connectUser = userFunctions.connectUser
const disconnectUser = userFunctions.disconnectUser

const createUserMessage = messageFunctions.createUserMessage
const createServerMessage = messageFunctions.createServerMessage

io.on("connect", (socket) => {
  try{
    username = getSocketUsernsame(socket)
    console.log(`${username} requesting connection`)
    socket.user = connectUser(username)
    createSocketListeners(socket)
  }catch(error){
    console.error(error)
    socket.disconnect(true)
  }

})

function getSocketUsernsame(socket){
  if(socket.handshake.query.username){
    return socket.handshake.query.username
  }else if(socket.handshake.headers.botaccount){
    return socket.handshake.headers.botaccount
  }
  throw("Couldnt identify user in socket, disconnecting")
}


function createSocketListeners(socket) {

  socket.emit('message', createServerMessage("Welcome to the Chat!"))
  io.emit('message', createServerMessage(`${socket.user.username} connected`));
  io.emit('userUpdate', socket.user);

  socket.on("message", message => {
    let messageObj = createUserMessage(socket.user, message)
    io.emit('message',messageObj);
  })

  socket.on('disconnect', (reason) => {
    io.emit('message', createServerMessage(`${socket.user.username} disconnected`));
    try {
      socket.user = disconnectUser(socket.user.username)
      io.emit('userUpdate', socket.user);
    } catch (err) {
      console.error(err)
    }
  })
}









module.exports = io