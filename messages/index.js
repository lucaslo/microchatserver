let messages = []

function createServerMessage(messageText) {
    let messageObj = { from: "server", text: messageText, timeStamp:Date.now()}
    messages.push(messageObj)
    return messageObj
}

function createUserMessage(user, message) {
    let messageObj = { from: user.username, text: message, timeStamp: Date.now() }
    messages.push(messageObj)
    return messageObj 
}

module.exports = {
    createServerMessage,
    createUserMessage
}