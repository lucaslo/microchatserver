var express = require('express');
var router = express.Router();
const userFunctions = require("../user")
const createUser = userFunctions.createUser
const getUsers = userFunctions.getUsers
/* GET users listing. */

router.get('/', function(req, res, next) {
  res.send({users:getUsers()});
});

router.post('/new', function(req, res, next) {
  let username = req.body.username
  try{
    user = createUser(username)
    res.send({users:getUsers(), token:{user:user, expiresIn:new Date(Date.now() + 30*60000)}})
  }catch{
    res.status(409).send({error:"Couldnt create user"})
  }

});

module.exports = router;
