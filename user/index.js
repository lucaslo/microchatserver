let users = []


function createUser (userName){
    let userAlreadyExists = users.findIndex(user => user.username===userName)
    if(userAlreadyExists!==-1){
       throw(`USER ${username} already being used`)
    }else{
        let newUser = {username:userName,status:"offline"}
        users.push(newUser)
        return newUser
    }
}

function connectUser(userName){
    let userIndex = users.findIndex((user)=> user.username===userName)
    if (userIndex==-1)
        throw(`${username} its not an user in the Users database`)
    else{
        let changeUser = {...users[userIndex], status:"online"}
        users[userIndex] = changeUser
    }
    
    return users[userIndex]
}

function disconnectUser(userName){
    let userIndex = users.findIndex((user)=> user.username===userName)
    if (userIndex==-1)
        throw (`no user with the username ${userName}`)
    else{
        users[userIndex] = {...users[userIndex], status:"offline"}
    }
    return users[userIndex]
}

function getUsers(){
    return users
}

function checkIfUserExists(username){
    let userIndex = users.findIndex((user)=> user.username===username)
    if (userIndex==-1){
        return false
    }

    return true
}

module.exports={
    createUser,
    connectUser,
    disconnectUser,
    getUsers,
    checkIfUserExists
}